# Use a imagem base do Python
FROM python:3.9-slim

# Customização mínima: Definir um maintainer
LABEL maintainer="seuemail@example.com"

# Definir diretório de trabalho
WORKDIR /app

# Copiar o arquivo de requisitos e instalar dependências
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

# Copiar o código da aplicação
COPY . .

# Expor a porta da aplicação
EXPOSE 5000

# Comando para rodar a aplicação
CMD ["python", "app.py"]
