from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def form():
    return '''
        <form method="post" action="/submit">
            Matricula: <input type="text" name="matricula"><br>
            Nome: <input type="text" name="nome"><br>
            Curso: <input type="text" name="curso"><br>
            <input type="submit">
        </form>
    '''

@app.route('/submit', methods=['POST'])
def submit():
    matricula = request.form['matricula']
    nome = request.form['nome']
    curso = request.form['curso']
    return f'Matricula: {matricula}, Nome: {nome}, Curso: {curso}'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
